import com.spotify.pages.LoginPage;
import com.spotify.pages.MainMenu;
import com.spotify.pages.RadioPage;
import com.spotify.pages.Util;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.sikuli.script.Screen;

@RunWith(JUnit4.class)
public class MainTest {

    private static Screen screen = new Screen();

    @Test
    public void testCreateStation() throws InterruptedException {

        Util.openApplication(screen);

        LoginPage.enterUserCredentials(screen);
        LoginPage.clickOnLoginButton(screen);

        MainMenu.accessRadioPage(screen);

        RadioPage.clickCreateNewStation(screen);
        RadioPage.searchArtist(screen);
        RadioPage.selectTopResult(screen);
        RadioPage.clickOnPlayButton(screen);
        RadioPage.clickOnPauseButton(screen);

        Util.logoutApplication(screen);
        //Util.closeApplication(screen);

    }


}
