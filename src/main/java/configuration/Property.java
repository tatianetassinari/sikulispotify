package configuration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Property {
    private static String file;
    private static Property property;



    public static Property getInstance() {
        if (property == null) {
            property = new Property();
        }

        return property;
    }


    public String getProperty(String property) {
        Properties properties = new Properties();
        String result = null;
        FileInputStream input;
        try {
            input = new FileInputStream(file);
            properties.load(input);
            result = properties.getProperty(property);
            input.close();
        } catch (IOException exception) {
            // exception.getMessage();
            return null;
        }

        return result;
    }

    public Boolean setProperty(String key, String value) {
        Properties properties = new Properties();

        try {
            properties.load(new FileInputStream(file));
        } catch (IOException exception) {
            // exception.getMessage();
            return false;
        }

        FileOutputStream output;
        try {
            output = new FileOutputStream(file);
            properties.setProperty(key, value);
            properties.store(output, "");
            output.close();
        } catch (FileNotFoundException exception) {
            return false;
        } catch (IOException exception) {
            return false;
        }

        return true;
    }

    public Boolean removeProperty(String key) {
        Properties properties = new Properties();
        FileInputStream input;

        try {
            input = new FileInputStream(file);
            properties.load(input);
            properties.remove(key);
            properties.store(new FileOutputStream(file), null);
            input.close();
        } catch (IOException exception) {
            // exception.getMessage();
            return false;
        }

        return true;
    }

    public class getInstance extends Property {
    }
}