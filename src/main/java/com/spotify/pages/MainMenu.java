package com.spotify.pages;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;


public class MainMenu {

    Screen screen;

    public static Pattern RADIO_BTN;

    static {
        RADIO_BTN = new Pattern(LoginPage.class.getClassLoader().getResource("MainMenu/Radio.png")).similar((float) 0.7);
    }

    public MainMenu(Screen screen) {
        this.screen = screen;

    }

    public static void accessRadioPage(Screen screen) {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            screen.wait(RADIO_BTN, 7);
            screen.find(RADIO_BTN);
            screen.mouseMove(RADIO_BTN);
            screen.click(RADIO_BTN);

        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }
    }


}
