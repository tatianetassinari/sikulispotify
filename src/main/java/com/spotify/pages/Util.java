package com.spotify.pages;

import org.sikuli.script.App;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class Util {

    private static Screen screen;


    public static Pattern TOP_MENU;
    public static Pattern LOGOUT_BTN;
    public static Pattern LOGIN_PAGE;

    static {
        TOP_MENU = new Pattern(RadioPage.class.getClassLoader().getResource("Util/topMenu.png")).similar((float) 0.7);
    }

    static {
        LOGOUT_BTN = new Pattern(RadioPage.class.getClassLoader().getResource("Util/logout.png")).similar((float) 0.7);
    }

    static {
        LOGIN_PAGE = new Pattern(RadioPage.class.getClassLoader().getResource("Util/LoginPage.png")).similar((float) 0.7);
    }

    public Util(Screen screen) {
        this.screen = new Screen();

    }

    public static void openApplication(Screen screen) {
        String appPath = System.getProperty("appPath");
        App.open(appPath);

    }

    public static void logoutApplication(Screen screen) {
        try {
            screen.find(TOP_MENU);
            screen.wait(TOP_MENU, 5).click();
            screen.find(LOGOUT_BTN);
            screen.wait(LOGOUT_BTN, 7).click();
            //Login page is displayed
            screen.exists(LOGIN_PAGE);

        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }
    }

    public static void closeApplication(Screen screen) {
        App.close("Spotify");
    }

}
