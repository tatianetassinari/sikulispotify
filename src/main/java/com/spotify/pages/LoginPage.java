package com.spotify.pages;

import org.sikuli.script.*;

public class LoginPage {

    private static Screen screen;

    public static Pattern LOGIN_BUTTON;
    public static Pattern EMAIL_FIELD;
    public static Pattern PASSWORD_FIELD;

    static {
        // ImagePath.add("C://Users//ttassin//workspace//sikuliTest//src//main//resources/");
        LOGIN_BUTTON = new Pattern(LoginPage.class.getClassLoader().getResource("LoginPage/LoginBtn.png")).similar((float) 0.7);
    }

    static {
        EMAIL_FIELD = new Pattern(LoginPage.class.getClassLoader().getResource("LoginPage/Email.png")).similar((float) 0.7);
    }

    static {
        PASSWORD_FIELD = new Pattern(LoginPage.class.getClassLoader().getResource("LoginPage/Password.png")).similar((float) 0.7);
    }

    public LoginPage(Screen screen) {
        this.screen = new Screen();

    }

    public static void enterUserCredentials(Screen screen) {
        String username = System.getProperty("username");
        String password = System.getProperty("password");

        try {

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            screen.wait(LOGIN_BUTTON, 10);

            screen.type("a", KeyModifier.CTRL);
            screen.type(Key.BACKSPACE);
            screen.type(Key.DELETE);

            screen.click(EMAIL_FIELD);
            screen.type(EMAIL_FIELD, username);

            screen.wait(PASSWORD_FIELD, 7);

            screen.mouseMove(PASSWORD_FIELD);
            screen.click(PASSWORD_FIELD);
            screen.type(PASSWORD_FIELD, password);
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }


    }


    public static void clickOnLoginButton(Screen screen) {
        //Debug.setDebugLevel(3);

        try {
            screen.find(LOGIN_BUTTON).click();
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }
    }



}
