package com.spotify.pages;

import org.sikuli.script.App;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;


public class RadioPage {

    private static Screen screen;

    public static Pattern CREATE_NEW_STATION;
    public static Pattern SEARCH_STATION;
    public static Pattern CHOSEN_STATION;
    public static Pattern PLAY_BTN;
    public static Pattern PAUSE_BTN;
    public static Pattern ARTIST_RADIO;


    static {
        CREATE_NEW_STATION = new Pattern(RadioPage.class.getClassLoader().getResource("RadioPage/createNewStation.png")).similar((float) 0.7);
    }

    static {
        SEARCH_STATION = new Pattern(RadioPage.class.getClassLoader().getResource("RadioPage/searchStation.png")).similar((float) 0.7);
    }

    static {
        CHOSEN_STATION = new Pattern(RadioPage.class.getClassLoader().getResource("RadioPage/radiohead.png")).similar((float) 0.7);
    }

    static {
        PLAY_BTN = new Pattern(RadioPage.class.getClassLoader().getResource("RadioPage/play.png")).similar((float) 0.7);
    }

    static {
        PAUSE_BTN = new Pattern(RadioPage.class.getClassLoader().getResource("RadioPage/pause.png")).similar((float) 0.7);
    }

    static {
        ARTIST_RADIO = new Pattern(RadioPage.class.getClassLoader().getResource("RadioPage/artistRadio.png")).similar((float) 0.7);
    }



    public RadioPage(Screen screen) {
        this.screen = new Screen();

    }

    public static void clickCreateNewStation(Screen screen) throws InterruptedException {

        Thread.sleep(5000);

        try {
            //screen.wait(CREATE_NEW_STATION);
            screen.find(CREATE_NEW_STATION);
            //screen.mouseMove((CREATE_NEW_STATION));
            screen.click(CREATE_NEW_STATION);

        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }

    }

    public static void searchArtist(Screen screen) {
        try {
            screen.find(SEARCH_STATION);
            screen.wait(SEARCH_STATION, 5).type("radiohead");
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }
    }

    public static void selectTopResult(Screen screen) {
        try {
            screen.find(CHOSEN_STATION);
            screen.wait(CHOSEN_STATION, 5).click();
            //***
            // The Artist Radio Is Displayed To The User
            //***
            screen.exists(ARTIST_RADIO);
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }
    }

    public static void clickOnPlayButton(Screen screen) {
        try {
            screen.find(PLAY_BTN);
            screen.wait(PLAY_BTN, 5).click();
            //***
            //The Music Starts To Play
            //***
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }
    }

    public static void clickOnPauseButton(Screen screen) throws InterruptedException {
        Thread.sleep(10000);

        try {
            screen.find(PAUSE_BTN);
            screen.wait(PAUSE_BTN, 5).click();

        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }
    }





}



